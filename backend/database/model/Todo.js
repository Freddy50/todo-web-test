// Importing module
import pkg from "mongoose";
const { model, Schema } = pkg;

export const DOCUMENT_NAME = "Toto";
export const COLLECTION_NAME = "todos";

// Create Schema Instance for Todo and add properties
const schema = new Schema(
  {
    title: {
      type: String,
      required: [true, "can't be blank"],
    },
    description: {
      type: String,
    },
    done: { type: Boolean, default: false },
    dueDate: {
      type: String,
    },
  },
  { timestamps: true },
  { versionKey: false }
);

export const Todo = model(DOCUMENT_NAME, schema, COLLECTION_NAME);
